Proof of concept software for detecting meteor showers
---

Entry for [ESA summer of code](http://sophia.estec.esa.int/socis) for the [MLAB Video meteor detection system ](http://wiki.mlab.cz/doku.php?id=en:socis2014#video_meteor_detection_system)

It currently just filters the data by pixels deviating from the mean with more than one standard-deviation and then applies OpenCV contour detection.

In a later stage it should use some kind of fall-off for the mean and standard deviation to cope with changing light and weather. It should also attempt to follow a meteor, plot a line across the collected data-points and then look at the area where the meteor probably came from in the preceding frames with a lower threshold in order to detect it earlier. Other enhancements include threshold for the least number of hits in a line to consider it worthy of reporting as a possible meteor and manipulation of the  deviation-threshold in case of false positives or missed faint starts of the meteor.

Also re-writing it in Java or C is probably necessary to process more than VGA-resolution in real-time.

If time allows feature extraction and possible machine learning might be applied to further classify the meteors.

Here shown capturing the first meteor fall in the video [Perseids 2013 YouTube ](https://www.youtube.com/watch?v=anhidIYuEYI)

![example rendition](/Zinob/meteor-demo/raw/master/ex.jpg)
(created with an older version but just minor graphical changes)

Further analysis:
===
It is how ever apparent that the algorithm is not quite so flawless as it appears from the first example when analysing  [the other events in the video](/Zinob/meteor-demo/raw/master/ex3.jpg), especially the last one, frames labelled 56-58 where it only picks up the very last flare. That is a typical example of where I suspect that the only reliable method is a spatially local relaxation of the detection criteria in previous frames upon finding a suspected meteor.