### This code is allegedly hideous, it is just a proof of concept/early test
import numpy as np
import cv2
from matplotlib import pyplot as plt

def centroid(cnt):
    m=cv2.moments(cnt)
    if len(cnt)>2:
        m00=m['m00']
        return [int(m['m10']/m00),int(m['m01']/m00),int(m00)]
    else:
        return [int(i) for i in cnt.flat[0:2]]+[1]

def plotBestFit(img,contours):
    centers=np.int32(np.array(contours)[:,0:2])
    if len(contours)> 1:
        x = centers[:,0]
        y = centers[:,1]
        w,h= img.shape[:2]
        A= np.vstack([x, np.ones(len(x))]).T
        m, c = np.linalg.lstsq(A, y)[0]
        #startx 0=x*m+c -c/m=x startx, ...rly?
        #endx h=x*m+c => h-c=x*m =>  (h-c)/m =x
        cv2.line(img,(int(-c/m),0),(int((h-c)/m),h),(255,0,0,128),1)

    cv2.line(img,tuple(centers[0]),tuple(centers[-1]),(0,0,255),1)

kernel = np.ones((5,5),np.uint8)
frames=[]
for i in range(41,60)+range(231,254)+range(512,538):
    f=cv2.imread("C:\Users\Zinob\Videos\meteors\images\\full\image%06i.png"%i)
    frames.append(f[10:440, 10:630])    #Trim high noise areas, edges, clock.
   
gray=cv2.cvtColor(frames[0],cv2.COLOR_BGR2GRAY)
Stdev=np.zeros(gray.shape)
Mean=np.int16(gray)
k=0
Mask=Stdev

font = cv2.FONT_HERSHEY_SIMPLEX

sep=np.ones([100,frames[0].shape[1],3])*255
cv2.putText(sep,"New frame-set",(0,30), font, 1,(255,0,0),2)

trail=[]
mFrames=[Stdev,Stdev]
res=[]
for f in frames:
    gray=cv2.cvtColor(f,cv2.COLOR_BGR2GRAY)
    k+=1
    Mean=(Mean+gray)/2  #Actually not mean or even sliding average but faster to compute
                        #Should probbably use a slower fall off than since 1/2:(1/4+1/8+1/16..)
                        #will put to much weight at recent frames
    Mask=np.greater(gray-Mean, Stdev)*250
    contours, hierarchy = cv2.findContours(np.uint8(Mask),cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
    cv2.drawContours(f, contours, -1, (0,255,0), 3)
    centroids=[centroid(i) for i in contours]
    cv2.putText(f,str(k-1)+str(centroids),(0,20), font, .5,(255,255,255))
    #print "centroids:",centroids, " contours:",contours
    mFrames.append(f)
    if (centroids):
        trail.append(centroids[0])

    elif (trail):
        print trail
        for i in mFrames:
            plotBestFit(i,trail)
        res.extend(mFrames)
        res.append(sep)
        trail=[]
        mFrames=mFrames[:2]
    else:
        mFrames.pop(0)

        
    Stdev=Stdev+(gray-Stdev)/k  #This needs to have a falloff as well in order to match the Mean
    #print Stdev.max(), f.max(), Mean.max(), (f-Mean).max()
res.pop(-1)


cv2.imwrite('C:\Users\Zinob\Videos\meteors\ex3.png',np.vstack(res))
cv2.imwrite('C:\Users\Zinob\Videos\meteors\ex.png',np.vstack(res[:8]))
cv2.destroyAllWindows()
