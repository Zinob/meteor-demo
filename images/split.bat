REM Creates two folders.
REM /img/ quick browsing but with a rather aggressive duplicate frame removal and slightly lower image quality
REM /fill/ with removal of only completely identical frames and high-quality png. 

mkdir img
mkdir full

del /Q img\*
del /Q full\*

ffmpeg -y -i perseids_2013_large.mp4 -vf mpdecimate,decimate -qscale 0 "img/image%%6d.jpg" 
ffmpeg -y -i perseids_2013_large.mp4 -vf decimate -qscale 0 "full/image%%6d.png" 

REM c:\Program Files (x86)\IrfanView\i_view32.exe C:\Users\Zinob\Videos\meteors\img\image000001.jpg